package gui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

public class OpretKundePane extends GridPane {
	String dbURL = "jdbc:sqlserver://mssql4.gear.host";
	String user = "skole2";
	String pass = "Lr3CUV7648~~";

	private Label lblCpr;
	private Label lblNavn;
	private Label lblAdresse;
	private Label lblByNavn;
	private Label lblRegNr;
	private Label lblKtoNr;
	private Label lblSaldo;
	private Label lblLåneret;
	private Label lblKunder;
	private RadioButton rbLøn = new RadioButton("Løn");
	private RadioButton rbPrioritet = new RadioButton("Prioritet");
	private TextField txfCPRnr;
	private TextField txfNavn;
	private TextField txfAdresse;
	private TextField txfPostNr;
	private TextField txfByNavn;
	private TextField txfRegNr;
	private TextField txfKtoNr;
	private TextField txfSaldo;
	private TextField txfLåneret;
	private Button btnKunde;
	private Button btnKonto;
	private ListView<String> lvwKunder;
	private ListView<String> lvwSaldo;
	private ListView<String> lvwTekst;
	ArrayList<String> kunder = new ArrayList<>();
	ArrayList<String> konti = new ArrayList<>();

	public OpretKundePane() {

		setPadding(new Insets(20));
		setHgap(20);
		setVgap(10);
		setGridLinesVisible(false);

		lblCpr = new Label("CPR nummer:");
		this.add(lblCpr, 0, 0);
		txfCPRnr = new TextField();
		this.add(txfCPRnr, 1, 0, 2, 1);

		lblNavn = new Label("Navn: ");
		this.add(lblNavn, 0, 1);
		txfNavn = new TextField();
		this.add(txfNavn, 1, 1, 2, 1);

		lblAdresse = new Label("Adresse: ");
		this.add(lblAdresse, 0, 2);
		txfAdresse = new TextField();
		this.add(txfAdresse, 1, 2, 2, 1);

		lblByNavn = new Label("By: ");
		this.add(lblByNavn, 0, 3);
		txfByNavn = new TextField();
		this.add(txfByNavn, 1, 3);
		txfPostNr = new TextField();
		this.add(txfPostNr, 2, 3);
		txfPostNr.setPrefWidth(70);

		lblRegNr = new Label("Reg. Nummer: ");
		this.add(lblRegNr, 0, 4);
		txfRegNr = new TextField();
		this.add(txfRegNr, 1, 4);

		lblKtoNr = new Label("Konto Nummer: ");
		this.add(lblKtoNr, 0, 5);
		txfKtoNr = new TextField();
		this.add(txfKtoNr, 1, 5);

		lblSaldo = new Label("Saldo: ");
		this.add(lblSaldo, 0, 6);
		txfSaldo = new TextField();
		this.add(txfSaldo, 1, 6);

		rbLøn = new RadioButton("Løn");
		this.add(rbLøn, 0, 7);
		rbPrioritet = new RadioButton("Prioritet");
		this.add(rbPrioritet, 1, 7);

		lblLåneret = new Label("Låneret");
		this.add(lblLåneret, 0, 8);
		txfLåneret = new TextField();
		this.add(txfLåneret, 1, 8);

		btnKunde = new Button("Opret Kunde");
		this.add(btnKunde, 0, 9);
		// btnKunde.setOnAction(event -> opretKunde());

		btnKonto = new Button("Opret Konto");
		this.add(btnKonto, 1, 9);
		// btnKonto.setOnAction(event -> opretKonto());

		VBox vbListView = new VBox();
		this.add(vbListView, 3, 1, 5, 10);

		lblKunder = new Label("Kunder");
		this.add(lblKunder, 3, 0);

		lvwKunder = new ListView<>();
		lvwKunder.setPrefHeight(287);
		lvwKunder.setPrefWidth(200);

		vbListView.getChildren().add(lvwKunder);

		lvwKunder.getSelectionModel().selectedItemProperty().addListener(observable -> {
			updateKundeInfo();
		});

	}

	public void updateControls() {
		btnKunde.setOnAction(event -> opretKunde());
		btnKonto.setOnAction(event -> opretKonto());
		lvwKunder.getItems().clear(); // Forstår virkelig ikke hvorfor at den
										// ikke kan lave clear.
		try {
			Connection minConnection;
			String dbURL = "jdbc:sqlserver://mssql4.gear.host";
			String user = "skole2";
			String pass = "Lr3CUV7648~~";
			minConnection = DriverManager.getConnection(dbURL, user, pass);
			Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			ResultSet res = stmt.executeQuery("SELECT * From Kunde");
			while (res.next()) {
				kunder.add(res.getString("Navn"));
				lvwKunder.getItems().setAll(kunder);
			}
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}

	public void updateKundeInfo() {
		try {
			String kundeId = lvwKunder.getSelectionModel().getSelectedItem();
			Connection minConnection;
			String dbURL = "jdbc:sqlserver://mssql4.gear.host";
			String user = "skole2";
			String pass = "Lr3CUV7648~~";
			minConnection = DriverManager.getConnection(dbURL, user, pass);
			Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			Statement stmt2 = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Statement stmt3 = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			Statement stmt4 = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
					ResultSet.CONCUR_UPDATABLE);
			ResultSet res = stmt.executeQuery("SELECT * From Kunde WHERE navn = '" + kundeId + "'");
			while (res.next()) {
				txfNavn.setText(res.getString("Navn"));
				txfCPRnr.setText(res.getString("cprNr"));
				txfAdresse.setText(res.getString("adresse"));
				txfPostNr.setText(res.getString("postNr"));
				String postNr = res.getString("postNr");
				String cprNr = res.getString("cprNr");
				ResultSet res2 = stmt2.executeQuery("SELECT * From PostDistrikt WHERE postNr = '" + postNr + "'");
				while (res2.next()) {
					txfByNavn.setText(res2.getString("byNavn"));
					ResultSet res3 = stmt3.executeQuery("SELECT * From KundeHarKonto WHERE cprNr = '" + cprNr + "'");
					while (res3.next()) {
						txfRegNr.setText(res3.getString("regNr"));
						txfKtoNr.setText(res3.getString("ktonr"));
						String kontoNr = res3.getString("ktonr");
						ResultSet res4 = stmt4.executeQuery("SELECT * From Konto WHERE ktoNr = '" + kontoNr + "'");
						while (res4.next()) {
							txfSaldo.setText(res4.getString("saldo"));
						}
					}
				}
			}
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}

	private void opretKunde() {
		String cpr = txfCPRnr.getText();
		String navn = txfNavn.getText();
		String adresse = txfAdresse.getText();
		String postNr = txfPostNr.getText();

		if (!navn.isEmpty() && !adresse.isEmpty() && !postNr.isEmpty() && !cpr.isEmpty()) {
			try {
				Connection minConnection;
				String dbURL = "jdbc:sqlserver://mssql4.gear.host";
				String user = "skole2";
				String pass = "Lr3CUV7648~~";
				minConnection = DriverManager.getConnection(dbURL, user, pass);
				Statement stmt = minConnection.createStatement();
				stmt.executeUpdate("INSERT INTO Kunde " + "VALUES ('" + cpr + "','" + navn + "','" + adresse + "','"
						+ postNr + "')");
				System.out.println("Kunde Oprettet");

			} catch (Exception e) {
				System.out.println("fejl:  " + e.getMessage());
			}
			updateControls();
		}

	}

	private void opretKonto() {
		String cpr = txfCPRnr.getText();

		String regNr = txfRegNr.getText();
		String KtoNr = txfKtoNr.getText();
		String saldo = txfSaldo.getText();

		if (!regNr.isEmpty() && !KtoNr.isEmpty() && !saldo.isEmpty()) {
			try {
				Connection minConnection;
				String dbURL = "jdbc:sqlserver://mssql4.gear.host";
				String user = "skole2";
				String pass = "Lr3CUV7648~~";
				minConnection = DriverManager.getConnection(dbURL, user, pass);
				Statement stmt = minConnection.createStatement();
				stmt.executeUpdate("INSERT INTO Konto " + "VALUES('" + regNr + "','" + KtoNr + "','Test','" + saldo
						+ "','1','1')");
				stmt.executeUpdate(
						"INSERT INTO KundeHarKonto " + "VALUES('" + cpr + "','" + regNr + "','" + KtoNr + "')");
				System.out.println("Konto Oprettet");
			} catch (Exception e) {
				System.out.println("fejl:  " + e.getMessage());
			}
			// } Else fejl meddelsese

			updateControls();
		}

	}

}
