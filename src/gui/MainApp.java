package gui;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TabPane.TabClosingPolicy;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class MainApp extends Application {

	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage primaryStage) {

		BorderPane pane = new BorderPane();
		Scene scene = new Scene(pane);
		this.initContent(pane);

		primaryStage.setTitle("Andeby Bank");
		primaryStage.setScene(scene);
		primaryStage.show();

	}

	private void initContent(BorderPane pane) {
		
		TabPane tabPane = initTabPane();
		pane.setCenter(tabPane);
	}

	private TabPane initTabPane() {
		TabPane tabPane = new TabPane();
		tabPane.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

		// Tab Kunde
		Tab tabKunde = new Tab("Opret Kunde");
		tabPane.getTabs().add(tabKunde);
		OpretKundePane kundePane = new OpretKundePane();
		tabKunde.setContent(kundePane);
		tabKunde.setOnSelectionChanged(event -> kundePane.updateControls());
		kundePane.updateControls();

		return tabPane;
	}

}
